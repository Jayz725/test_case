import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider(
        create: (context) =>
        AuthBlocCubit(
        )
          ..checkAuth(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
        builder: (context, state)
        {
          if(state is AuthBlocNotLoginState)
          {
            return LoginPage();
          }
          
          if(state is AuthBlocLoadedLoginState)
          {
            return BlocProvider(
              create: (context) =>
              HomeBlocCubit(
              )
                ..fetchingData(),
              child: HomeBlocScreen(),
            );
          }

          if(state is AuthBlocInitialState) {
            return Scaffold(
              body: Center(
                child: Text("LOGO 1")
              )
            );
          }

          return Center(child: Text(
              kDebugMode?"state not implemented $state": ""
          ));
        });
  }
}

