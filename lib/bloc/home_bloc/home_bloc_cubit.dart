import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';

import 'package:majootestcase/utils/custom_exception.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());
    try {
      ApiServices apiServices = ApiServices();
      MovieModel movieResponse = await apiServices.getMovieList();
      if(movieResponse==null){
        emit(HomeBlocErrorState("Error Unknown"));
      }else{
        emit(HomeBlocLoadedState(movieResponse.results));
      }
    } on BadRequestException catch (b) {
      emit(HomeBlocErrorState(b.message));
    } on UnexpectedException catch (u) {
      emit(HomeBlocErrorState(u.message));
    }
  }
}
