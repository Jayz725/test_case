part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
    List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState { }

class AuthBlocNotLoginState extends AuthBlocState { }

class AuthBlocLoadingState extends AuthBlocState { }

class AuthBlocLoadedLoginState extends AuthBlocState {
    final data;

    AuthBlocLoadedLoginState(this.data);

    @override
    List<Object> get props => [data];
}

class AuthBlocLoadedRegisterState extends AuthBlocState {
    final message;

    AuthBlocLoadedRegisterState(this.message);

    @override
    List<Object> get props => [message];
}

class AuthBlocLoadedlogoutState extends AuthBlocState {
    final message;

    AuthBlocLoadedlogoutState(this.message);

    @override
    List<Object> get props => [message];
}

class AuthBlocErrorState extends AuthBlocState {
    final error;

    AuthBlocErrorState(this.error);

    @override
    List<Object> get props => [error];
}
