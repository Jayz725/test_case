import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/services/local/preferences_enum.dart';
import 'package:majootestcase/utils/custom_exception.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  // static AuthBlocCubit _authBlocCubit;
  AuthBlocCubit() : super(AuthBlocInitialState());

  // factory AuthBlocCubit.init() {
  //   if (_authBlocCubit == null){
  //     _authBlocCubit = AuthBlocCubit._();
  //   }

  //   return _authBlocCubit;
  // }

  // static void dispose() {
  //   _authBlocCubit.close();
  //   _authBlocCubit = null;
  // }

  void checkAuth() async{
    try {
      String email = await SimplePreferences.getPreferences(PreferencesEnum.EMAIL);
      if(email == null){
        emit(AuthBlocNotLoginState());
      } else {
        User user = await ApiServices().getUser(email);
        emit(AuthBlocLoadedLoginState(user));
      }
    } on SqlNoResultException {
      emit(AuthBlocNotLoginState());
    } on SqlException {
      emit(AuthBlocNotLoginState());
    }
  }

  void loginUser(User user) async{
    try {
      emit(AuthBlocLoadingState());
      User result = await ApiServices().getUser(user.email);
      if(result.password == user.password){
        await SimplePreferences.setPrefernces(PreferencesEnum.EMAIL, user.email);
        emit(AuthBlocLoadedLoginState(result));
      } else {
        emit(AuthBlocErrorState("Password Salah"));
      }
    } on SqlNoResultException {
      emit(AuthBlocErrorState("Email Tidak Ditemukan"));
    } on SqlException catch (s) {
      print("BAJING");
      emit(AuthBlocErrorState(s.message));
    }
  }

  void registerUser(User user) async {
    try {
      emit(AuthBlocLoadingState());
      String result = await ApiServices().register(user);
      await SimplePreferences.setPrefernces(PreferencesEnum.EMAIL, user.email);
      emit(AuthBlocLoadedRegisterState(result));
    } on SqlNoResultException {
      emit(AuthBlocErrorState("Gagal Mendaftarkan User"));
    } on SqlException catch (s) {
      emit(AuthBlocErrorState(s.message));
    }
  }

  void logoutUser() async{
    emit(AuthBlocLoadingState());
    await SimplePreferences.deletePreferences(PreferencesEnum.EMAIL);
    emit(AuthBlocLoadedlogoutState("Berhasil Logout"));
  }
}
