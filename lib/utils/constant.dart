import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:uuid/uuid.dart';

class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const API_KEY = "0bc9e6490f0a9aa230bd01e268411e10";
  static const BASE_URL = "https://api.themoviedb.org/3";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class StringManipulator {
  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static String generatedUuid(){
    return Uuid().v1();
  }
}
