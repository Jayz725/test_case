class UnauthorizedException implements Exception {
  const UnauthorizedException();
}

class UnexpectedException implements Exception {
  final String message;

  const UnexpectedException([this.message]);

   String toString() {
    Object message = this.message;
    if (message == null) return "Unexpected Exception";
    return "Unexpected Exception: $message";
  }

}

class BadRequestException implements Exception {
  final String message;

  const BadRequestException([this.message]);

  String toString() {
    Object message = this.message;
    if (message == null) return "Exception";
    return "Exception: $message";
  }

}

class SqlException implements Exception {
  final String message;

  const SqlException([this.message]);

  String toString() {
    Object message = this.message;
    if (message == null) return "SQL Exception";
    return "SQL Exception: $message";
  }
}

class SqlNoResultException implements Exception {
  const SqlNoResultException();
}