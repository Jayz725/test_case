import 'package:majootestcase/utils/constant.dart';

class User{
  final String email;
  final String userName;
  String _password;

  User({this.email, this.userName});

  String get password{
    return _password;
  }

  bool verifyPassword(String compare){
    return StringManipulator.generateMd5(compare) == _password;
  }

  void setPassword(String input){
    _password = StringManipulator.generateMd5(input);
  }

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        _password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': _password,
    'username' : userName
  };
}