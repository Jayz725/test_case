import 'package:flutter/material.dart';

Future<bool> showChoiceDialog(BuildContext context, {String title, String description, Function() yesChoice, Function() noChoice}) async {

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (container) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: WillPopScope(
            onWillPop: () async => false,
            child: Center(
              child: Container(
                width: 366 *  0.575,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16)
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(title != null ? title : "Logout"),
                    SizedBox(height: 20),
                    Text(description != null ? description : "Apakah Anda Yakin"),
                    SizedBox(height: 30),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: FlatButton(
                            height: 40,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.blue, width: 2),
                              borderRadius: BorderRadius.circular(25.0)
                            ),
                            onPressed: () => noChoice == null ? Navigator.of(context).pop(false) : noChoice(), 
                            child: Text("Tidak", style: TextStyle(color: Colors.blue))
                          ),
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: FlatButton(
                            height: 40,
                            color: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                            onPressed: () => yesChoice == null ? Navigator.of(context).pop(true) : yesChoice(), 
                            child: Text("Iya", style: TextStyle(color: Colors.white))
                          ),
                        )
                      ]
                    )
                  ]
                )
              ),
            ),
          ),
        );
      }
    );
  }