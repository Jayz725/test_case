import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';

Future<dynamic> showBottomWidgetMessage(BuildContext context, {String message, bool isSuccess, Function() action}) {

  return showModalBottomSheet(
    context: context, 
    isScrollControlled: true,
    enableDrag: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(top: Radius.circular(26)),
    ),
    builder: (container) {
      return Padding(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: MediaQuery.of(context).viewInsets.bottom
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 50),
            isSuccess != null ? isSuccess ? Icon(Icons.check_circle_outline, size: 120, color: Colors.green) : Icon(Icons.cancel_outlined, size: 120, color: Colors.red) : Text("Logo 2"),
            SizedBox(height: 30),
            Text(message != null ? message : "Terjadi Kesalahan...."),
            SizedBox(height: 50),
            CustomButton(
              text: 'Ok',
              onPressed: () {
                Navigator.of(context).pop();
                if (action != null) action();
              },
              height: 100,
            ),
            SizedBox(height: 50)
          ]
        )
      );
    }
  );
}