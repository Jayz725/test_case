import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final double height;
  final Color color;

  const LoadingIndicator({Key key, this.height =10, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(strokeWidth: 100),
      ),
    );
  }
}

  Future<dynamic> showLoadingDialog(BuildContext context) async {

    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (container) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: WillPopScope(
            onWillPop: () async => false,
            child: Center(
              child: Container(
                width: 366 *  0.575,
                padding: EdgeInsets.all(30),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16)
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Mohon Tunggu"),
                    SizedBox(height: 40),
                    CircularProgressIndicator()
                  ]
                )
              ),
            ),
          ),
        );
      }
    );
  }
