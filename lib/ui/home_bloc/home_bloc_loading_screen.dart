import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HomeBlocLoadingScreen extends StatelessWidget {

  const HomeBlocLoadingScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 9 / 17,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10
            
          ),
          itemCount: 9,
          itemBuilder: (context, index) {
            return movieItemWidget();
          },
        ),
    );
  }

  Widget movieItemWidget(){
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AspectRatio(
            aspectRatio: 2/3,
            child: Shimmer.fromColors(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[50]
            )
          ),
          Padding(
            padding: EdgeInsets.only(top: 7, bottom: 4, left: 10, right: 10),
            child: Shimmer.fromColors(
              child: Container(
                width: 120,
                height: 14,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)
                )
              ),
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[50]
            )
          )
        ],
      ),
    );
  }
}
