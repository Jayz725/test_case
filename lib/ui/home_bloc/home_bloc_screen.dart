import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_loading_screen.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/choice_dialog.dart';
import 'package:majootestcase/ui/extra/bottom_widget_message.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthBlocCubit _authBloc = AuthBlocCubit();
     
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie DB"),
        elevation: 0,
        actions: [
          IconButton(icon: Icon(Icons.exit_to_app), onPressed: () async {
            if(await showChoiceDialog(context)) {
              _authBloc.logoutUser();
            } 
          })
        ],
      ),
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        cubit: _authBloc,
        listener: (context, state) {
          if(state is AuthBlocLoadingState){
            showLoadingDialog(context);
          }

          if(state is AuthBlocErrorState){
            Navigator.of(context).pop();
            showBottomWidgetMessage(context, message: state.error, isSuccess: false);
          }

          if(state is AuthBlocLoadedlogoutState){
            Navigator.popAndPushNamed(
              context,
              "/"
            );
          }
        },
        child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
          builder: (context, state) {

            if(state is HomeBlocLoadedState){
              return HomeBlocLoadedScreen(data: state.data);
            }

            if(state is HomeBlocLoadingState){
              return HomeBlocLoadingScreen();
            }

            if(state is HomeBlocInitialState){
              return Container();
            }

            if(state is HomeBlocErrorState){
              return ErrorScreen(message: state.error, retry: () => BlocProvider.of<HomeBlocCubit>(context).fetchingData());
            }

            return Center(child: Text(
              kDebugMode?"state not implemented $state": ""
            ));
          }
        )
      )
    );
  }
}
