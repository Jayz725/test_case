import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

import '../detail/detail_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.all(16.0),
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 9 / 17,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10
                
              ),
              itemCount: data.length,
              itemBuilder: (context, index) {
                return movieItemWidget(context, data[index]);
              },
            ),
          
      
    );
  }

  Widget movieItemWidget(BuildContext context, Results data){
    return Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)
          )
      ),
      child: InkWell(
        onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => DetailPage(data))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 2/3,
              child: Image.network("https://image.tmdb.org/t/p/w500"+data.posterPath),
            ),
            Padding(
              padding: EdgeInsets.only(top: 7, bottom: 4, left: 10, right: 10),
              child: Text(data.name ?? data.title, textDirection: TextDirection.ltr, maxLines: 2, overflow: TextOverflow.fade),
            )
          ],
        ),
      ),
    );
  }
}
