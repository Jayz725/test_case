import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/bottom_widget_message.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  final _confirmController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  AuthBlocCubit _authBlocCubit;

  bool _isObscurePassword = true;
  bool _isObscureConfPassword = true;

  @override
  void initState() {
    super.initState();
    _authBlocCubit = AuthBlocCubit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(onPressed: () => Navigator.of(context).pop(), icon: Icon(Icons.arrow_back_ios_outlined, color: ThemeData.light().textTheme.bodyText2.color)),
      ),
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        cubit: _authBlocCubit,
        listener: (context, state) {
          if(state is AuthBlocLoadingState){
            showLoadingDialog(context);
          }

          if(state is AuthBlocErrorState){
            Navigator.of(context).pop();
            showBottomWidgetMessage(context, message: state.error, isSuccess: false);
          }

          if(state is AuthBlocLoadedRegisterState){
            showBottomWidgetMessage(context, message: state.message, isSuccess: true, action: () =>
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(),
                    child: HomeBlocScreen(),
                  ),
                ),
              )
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 25, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Registrasi',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan isi form dibawah',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleregister,
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Username',
            label: 'Username'
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'Password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Confirm Password',
            hint: 'Confirm Password',
            controller: _confirmController,
            isObscureText: _isObscureConfPassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscureConfPassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscureConfPassword = !_isObscureConfPassword;
                });
              },
            ),
            validator: (val) {
              return _passwordController.value == val ? null : "Password Konfirmasi Harus Sama";
            },
          ),
        ],
      ),
    );
  }

  void handleregister() async {
    final _username = _usernameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _confirm = _confirmController.value;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null &&
        _confirm != null
       ) {

      User user = User(
          userName: _username,
          email: _email,
      )..setPassword(_password);
      _authBlocCubit.registerUser(user);
    }
  }
}
