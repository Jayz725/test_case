import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

class DetailPage extends StatelessWidget{
  final Results data;

  DetailPage(this.data, {Key key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 16/9,
                child: Image.network("https://image.tmdb.org/t/p/w500"+data.backdropPath)
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.title ?? data.name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      data.releaseDate ?? data.firstAirDate,
                      style: TextStyle(
                        color: Colors.grey[500],
                      ),
                    ),
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildButtonColumn(Colors.pink, Icons.favorite, data.voteAverage.toString(), secondary: data.voteCount.toString()),
                        _buildButtonColumn(Colors.amber, Icons.star, data.popularity.round().toString()),
                      ],
                    ),
                    SizedBox(height: 40),
                    Text(
                      'Summary',
                      style: TextStyle(fontSize:  14, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    Text(
                      data.overview,
                      softWrap: true,
                    )
                  ]
                )
              )
            ]
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height:  MediaQuery.of(context).size.height,
            child: Align(
              alignment: Alignment(-0.9, -0.9), 
              child: InkWell(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Ink(
                  width: 50,
                  decoration: BoxDecoration(
                    color: Colors.black87.withOpacity(1),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.arrow_back_ios_outlined, color: Colors.white),
                ),
              )
            ),
          )
          ]
        )
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label, {String secondary}) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: color,
            ),
          ),
        ),
        secondary != null ? Container(
          margin: const EdgeInsets.only(top: 4),
          child: Text(
            secondary,
            style: TextStyle(
              fontSize: 14,
              color: color,
            )
          )
        ) : Container()
      ]
    );
  }

}