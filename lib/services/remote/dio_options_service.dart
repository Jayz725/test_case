import 'package:dio/dio.dart';
import 'package:majootestcase/services/remote/dio_config_service.dart';

class DioOptionsService {

  static Dio _dio;
  static DioOptionsService _dioOptions;

  DioOptionsService._(){
    if(_dio == null){
      _dio = DioConfigService();
    }
  }

  static DioOptionsService init(){
    if(_dioOptions == null){
      _dioOptions = DioOptionsService._();
    }

    return _dioOptions;
  }

  Future<Response> getWithParam(String path, {Map<String, dynamic> param}) {
    return _dio.get(path, options : Options(headers: {"Content-Type": "application/json;"}),queryParameters: param);
  }

}