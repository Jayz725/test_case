import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioConfigService with DioMixin implements Dio {
  DioConfigService() {
    options = BaseOptions(
      baseUrl: Api.BASE_URL,
      connectTimeout: 10000,
      receiveTimeout: 10000
    );

    interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90
      )
    );

    httpClientAdapter = DefaultHttpClientAdapter();
  }

}

