import 'dart:async';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/custom_exception.dart';
import 'package:sqflite/sqflite.dart';
import 'app_database.dart';

class UserDao {
  static UserDao _userDao;

  UserDao._();

  Future<String> registerUser(User user) async {
    int result;

    Database query = await AppDatabase.instance.database;
    result = await query.rawInsert(
      'INSERT INTO ${AppDatabase.tableUser}(${AppDatabase.columnIdUser}, '
      '${AppDatabase.columnEmail}, ${AppDatabase.columnUsername}, '
      '${AppDatabase.columnPassword}) '
      'VALUES("${StringManipulator.generatedUuid()}","${user.email}",'
      '"${user.userName}","${user.password}")'
    );
    if(result < 1){
      throw SqlNoResultException();
    }

    return "User Terdaftar";
  }

  Future<User> getUser(String email) async {
    var result;

    Database query = await AppDatabase.instance.database;
    var exec = await query.rawQuery('SELECT * FROM ${AppDatabase.tableUser} WHERE ${AppDatabase.columnEmail} = "$email"');
    if(exec.length < 1){
      throw SqlNoResultException();
    }

    result = User.fromJson(exec[0]);

    return result;
  }

  Future<String> logoutUser(String email) async {
    int result;

    Database query = await AppDatabase.instance.database;
    result = await query.rawDelete(
      'DELETE FROM ${AppDatabase.tableUser} WHERE ${AppDatabase.columnEmail} = "$email"'
    );
    if(result < 1){
      throw SqlNoResultException();
    }

    return "User Berhasil Logout";
  }

  static UserDao instance() {
    if (_userDao == null) {
      _userDao = UserDao._();
    }

    return _userDao;
  }
}
