import 'package:shared_preferences/shared_preferences.dart';

enum PreferencesEnum {
  EMAIL
}


class SimplePreferences {
  
  static Future<String> getPreferences(PreferencesEnum pref) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String email = sharedPreferences.getString(pref.toString());

    return email;
  }

  static Future<bool> setPrefernces(PreferencesEnum pref, String value) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(pref.toString(), value);
  }

  static Future<bool> deletePreferences(PreferencesEnum pref) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.remove(pref.toString());
  }

}