import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class AppDatabase {
  static const _databaseName = "majoo.sqlite";
  static const _databaseVersion = 1;

  static const tableUser = 'user';
  static const columnIdUser = 'id';
  static const columnEmail = 'email';
  static const columnUsername = 'username';
  static const columnPassword = 'password';

  AppDatabase._privateConstructor();
  static final AppDatabase instance = AppDatabase._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE IF NOT EXISTS $tableUser (
        $columnIdUser TEXT PRIMARY KEY,
        $columnEmail TEXT UNIQUE,
        $columnUsername TEXT UNIQUE,
        $columnPassword TEXT
      )
    ''');
  }
}