import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/local/user_dao.dart';
import 'package:majootestcase/services/remote/dio_options_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/custom_exception.dart';

class ApiServices{

final DioOptionsService _dioOptions = DioOptionsService.init();
final UserDao _userDao = UserDao.instance();

  Future<User> getUser(String email) async {
    var _response = await _userDao.getUser(email);
    return _response;
  }

  Future<String> logout(String email) async {
    var _response = await _userDao.logoutUser(email);
    return _response;
  }

  Future<String> register(User user) async {
    var _response = await _userDao.registerUser(user);
    return _response;
  }

  Future<MovieModel> getMovieList() async {

    try {
      var _response = await _dioOptions.getWithParam("/trending/all/day", param: {"api_key" : Api.API_KEY});
      return MovieModel.fromJson(_response.data);
    } on DioError catch (d) {
      if(d.type == DioErrorType.RESPONSE) {
        if (d.response.statusCode == 401){
          throw UnauthorizedException();
        } else if(d.response.statusCode >= 400 && d.response.statusCode <= 499) {
          throw BadRequestException(d.response.data);
        } else {
          throw UnexpectedException("Terjadi Kesalahan");
        }
      } else {
        throw UnexpectedException("Check Koneksi Anda");
      }
    }
  }
}